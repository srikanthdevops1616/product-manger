package net.codejava;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AppController {

	@Autowired
	private ProductService service; 
	
	@Autowired
	private StudentRepository studentRepository; 
	
	@RequestMapping("/")
	public ModelAndView viewHomePage(Model model) {
		ModelAndView model1 = new ModelAndView();
		List<Product> listProducts = service.listAll();
		model.addAttribute("listProducts", listProducts);
		model1.setViewName("index");
		
		return model1;
	}

	@RequestMapping("/yahoo")
	public ModelAndView marker()
	{
		ModelAndView model=new ModelAndView();
		model.addObject("msg","Hello How R You?");
		model.setViewName("HelloPage");
		return model;
	}
	@RequestMapping("/new")
	public String showNewProductPage(Model model) {
		Product product = new Product();
		model.addAttribute("product", product);
		
		return "new_product";
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveProduct(@ModelAttribute("product") Product product) {
		service.save(product);
		
		return "redirect:/";
	}
	
	@RequestMapping("/edit/{id}")
	public ModelAndView showEditProductPage(@PathVariable(name = "id") int id) {
		ModelAndView mav = new ModelAndView("edit_product");
		Product product = service.get(id);
		mav.addObject("product", product);
		
		return mav;
	}
	
	@RequestMapping("/delete/{id}")
	public String deleteProduct(@PathVariable(name = "id") int id) {
		service.delete(id);
		return "redirect:/";		
	}
	

	@RequestMapping("/submission")
	public ModelAndView submitData(@RequestParam("studentName") String name,@RequestParam("studentHobby") String hobby) {
		ModelAndView mav = new ModelAndView("edit_product");
		System.out.println(name+"-----------"+hobby);
		
		Student s1= new Student();
		s1.setName(name);
		s1.setHobby(hobby);
		
		studentRepository.save(s1);
		
		
		System.out.println("Hurryy ....Data  Saved");
		mav.setViewName("index");
		
		return mav;
	}
}
